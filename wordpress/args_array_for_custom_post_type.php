<?php



array(

'label' => '',
'labels' => '',
'description' => '',
'public' => '',
'exclude_from_search' => '',
'publicly_queryable' => '',
'show_ui' => '',
'show_in_nav_menus' => '',
'show_in_menu' => '',
'show_in_admin_bar' => '',
'menu_position' => '',
'menu_icon' => '',
'capability_type' => '',
'capabilities' => array(
  'edit_post'          => '', 
  'read_post'          => '', 
  'delete_post'        => '', 
  'edit_posts'         => '', 
  'edit_others_posts'  => '', 
  'publish_posts'      => '',       
  'read_private_posts' => '', 
  'create_posts'       => '', 
	),
'map_meta_cap' => '',
'hierarchical' => '',
'supports' => array(
        'title' => '',
        'editor' => '',
        'author' => '',
        'thumbnail' => '',
        'excerpt' => '',
        'trackbacks' => '',
        'custom-fields' => '',
        'comments' => '',
        'revisions' => '',
        'page-attributes' => '',
        'post-formats' => ''
		),
'register_meta_box_cb' => '',
'taxonomies' => array(),
'has_archive' => '',
'rewrite' => array(
        'slug' => '',
        'with_front' => '',
        'feeds' => '',
        'pages' => '',
        'ep_mask' => ''
		),
'permalink_epmask' => '',
'query_var' => '',
'can_export' => '',
'delete_with_user' => '',
'show_in_rest' => '',
'rest_base' => '',
'rest_controller_class' => '',
'_builtin' => '',
'_edit_link' => '',
);